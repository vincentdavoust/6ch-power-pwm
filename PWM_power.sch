EESchema Schematic File Version 2
LIBS:VincentDavoust
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:special
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:chargeur-cache
EELAYER 27 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date "12 feb 2016"
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MOSFET_N Q4
U 1 1 5325801D
P 9900 1750
F 0 "Q4" H 9910 1920 60  0000 R CNN
F 1 "MOSFET_N" H 9910 1600 60  0000 R CNN
F 2 "~" H 9900 1750 60  0000 C CNN
F 3 "~" H 9900 1750 60  0000 C CNN
	1    9900 1750
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q2
U 1 1 53258033
P 8700 1750
F 0 "Q2" H 8710 1920 60  0000 R CNN
F 1 "MOSFET_N" H 8710 1600 60  0000 R CNN
F 2 "~" H 8700 1750 60  0000 C CNN
F 3 "~" H 8700 1750 60  0000 C CNN
	1    8700 1750
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q5
U 1 1 53258039
P 10550 1750
F 0 "Q5" H 10560 1920 60  0000 R CNN
F 1 "MOSFET_N" H 10560 1600 60  0000 R CNN
F 2 "~" H 10550 1750 60  0000 C CNN
F 3 "~" H 10550 1750 60  0000 C CNN
	1    10550 1750
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q1
U 1 1 5325803F
P 8100 1750
F 0 "Q1" H 8110 1920 60  0000 R CNN
F 1 "MOSFET_N" H 8110 1600 60  0000 R CNN
F 2 "~" H 8100 1750 60  0000 C CNN
F 3 "~" H 8100 1750 60  0000 C CNN
	1    8100 1750
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q3
U 1 1 53258045
P 9300 1750
F 0 "Q3" H 9310 1920 60  0000 R CNN
F 1 "MOSFET_N" H 9310 1600 60  0000 R CNN
F 2 "~" H 9300 1750 60  0000 C CNN
F 3 "~" H 9300 1750 60  0000 C CNN
	1    9300 1750
	1    0    0    -1  
$EndComp
$Comp
L MOSFET_N Q0
U 1 1 53258069
P 7500 1750
F 0 "Q0" H 7510 1920 60  0000 R CNN
F 1 "MOSFET_N" H 7510 1600 60  0000 R CNN
F 2 "~" H 7500 1750 60  0000 C CNN
F 3 "~" H 7500 1750 60  0000 C CNN
	1    7500 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 1950 10775 1950
Connection ~ 10000 1950
Connection ~ 9400 1950
Connection ~ 8800 1950
Connection ~ 8200 1950
Text GLabel 7300 1750 0    47   Input ~ 0
drv0
Text GLabel 7900 1750 0    47   Input ~ 0
drv1
Text GLabel 8500 1750 0    47   Input ~ 0
drv2
Text GLabel 9100 1750 0    47   Input ~ 0
drv3
Text GLabel 9700 1750 0    47   Input ~ 0
drv4
Text GLabel 10350 1750 0    47   Input ~ 0
drv5
$Comp
L R RA2
U 1 1 53258126
P 1350 1350
F 0 "RA2" V 1430 1350 40  0000 C CNN
F 1 "R" V 1357 1351 40  0000 C CNN
F 2 "~" V 1280 1350 30  0000 C CNN
F 3 "~" H 1350 1350 30  0000 C CNN
	1    1350 1350
	1    0    0    -1  
$EndComp
$Comp
L R RA1
U 1 1 5325812C
P 1350 1850
F 0 "RA1" V 1430 1850 40  0000 C CNN
F 1 "R" V 1357 1851 40  0000 C CNN
F 2 "~" V 1280 1850 30  0000 C CNN
F 3 "~" H 1350 1850 30  0000 C CNN
	1    1350 1850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR1
U 1 1 53258132
P 1350 2100
F 0 "#PWR1" H 1350 2100 30  0001 C CNN
F 1 "GND" H 1350 2030 30  0001 C CNN
F 2 "" H 1350 2100 60  0000 C CNN
F 3 "" H 1350 2100 60  0000 C CNN
	1    1350 2100
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 53258138
P 2300 1400
F 0 "R2" V 2380 1400 40  0000 C CNN
F 1 "R" V 2307 1401 40  0000 C CNN
F 2 "~" V 2230 1400 30  0000 C CNN
F 3 "~" H 2300 1400 30  0000 C CNN
	1    2300 1400
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5325813E
P 2300 1900
F 0 "R1" V 2380 1900 40  0000 C CNN
F 1 "R" V 2307 1901 40  0000 C CNN
F 2 "~" V 2230 1900 30  0000 C CNN
F 3 "~" H 2300 1900 30  0000 C CNN
	1    2300 1900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR6
U 1 1 53258144
P 2300 2150
F 0 "#PWR6" H 2300 2150 30  0001 C CNN
F 1 "GND" H 2300 2080 30  0001 C CNN
F 2 "" H 2300 2150 60  0000 C CNN
F 3 "" H 2300 2150 60  0000 C CNN
	1    2300 2150
	1    0    0    -1  
$EndComp
$Comp
L R R12
U 1 1 5325814A
P 2850 1400
F 0 "R12" V 2930 1400 40  0000 C CNN
F 1 "R" V 2857 1401 40  0000 C CNN
F 2 "~" V 2780 1400 30  0000 C CNN
F 3 "~" H 2850 1400 30  0000 C CNN
	1    2850 1400
	1    0    0    -1  
$EndComp
$Comp
L R R11
U 1 1 53258150
P 2850 1900
F 0 "R11" V 2930 1900 40  0000 C CNN
F 1 "R" V 2857 1901 40  0000 C CNN
F 2 "~" V 2780 1900 30  0000 C CNN
F 3 "~" H 2850 1900 30  0000 C CNN
	1    2850 1900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR7
U 1 1 53258156
P 2850 2150
F 0 "#PWR7" H 2850 2150 30  0001 C CNN
F 1 "GND" H 2850 2080 30  0001 C CNN
F 2 "" H 2850 2150 60  0000 C CNN
F 3 "" H 2850 2150 60  0000 C CNN
	1    2850 2150
	1    0    0    -1  
$EndComp
$Comp
L R R22
U 1 1 5325815C
P 3500 1400
F 0 "R22" V 3580 1400 40  0000 C CNN
F 1 "R" V 3507 1401 40  0000 C CNN
F 2 "~" V 3430 1400 30  0000 C CNN
F 3 "~" H 3500 1400 30  0000 C CNN
	1    3500 1400
	1    0    0    -1  
$EndComp
$Comp
L R R21
U 1 1 53258162
P 3500 1900
F 0 "R21" V 3580 1900 40  0000 C CNN
F 1 "R" V 3507 1901 40  0000 C CNN
F 2 "~" V 3430 1900 30  0000 C CNN
F 3 "~" H 3500 1900 30  0000 C CNN
	1    3500 1900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR8
U 1 1 53258168
P 3500 2150
F 0 "#PWR8" H 3500 2150 30  0001 C CNN
F 1 "GND" H 3500 2080 30  0001 C CNN
F 2 "" H 3500 2150 60  0000 C CNN
F 3 "" H 3500 2150 60  0000 C CNN
	1    3500 2150
	1    0    0    -1  
$EndComp
$Comp
L R R32
U 1 1 5325816E
P 4250 1400
F 0 "R32" V 4330 1400 40  0000 C CNN
F 1 "R" V 4257 1401 40  0000 C CNN
F 2 "~" V 4180 1400 30  0000 C CNN
F 3 "~" H 4250 1400 30  0000 C CNN
	1    4250 1400
	1    0    0    -1  
$EndComp
$Comp
L R R31
U 1 1 53258174
P 4250 1900
F 0 "R31" V 4330 1900 40  0000 C CNN
F 1 "R" V 4257 1901 40  0000 C CNN
F 2 "~" V 4180 1900 30  0000 C CNN
F 3 "~" H 4250 1900 30  0000 C CNN
	1    4250 1900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR9
U 1 1 5325817A
P 4250 2150
F 0 "#PWR9" H 4250 2150 30  0001 C CNN
F 1 "GND" H 4250 2080 30  0001 C CNN
F 2 "" H 4250 2150 60  0000 C CNN
F 3 "" H 4250 2150 60  0000 C CNN
	1    4250 2150
	1    0    0    -1  
$EndComp
$Comp
L R R42
U 1 1 53258180
P 4850 1400
F 0 "R42" V 4930 1400 40  0000 C CNN
F 1 "R" V 4857 1401 40  0000 C CNN
F 2 "~" V 4780 1400 30  0000 C CNN
F 3 "~" H 4850 1400 30  0000 C CNN
	1    4850 1400
	1    0    0    -1  
$EndComp
$Comp
L R R41
U 1 1 53258186
P 4850 1900
F 0 "R41" V 4930 1900 40  0000 C CNN
F 1 "R" V 4857 1901 40  0000 C CNN
F 2 "~" V 4780 1900 30  0000 C CNN
F 3 "~" H 4850 1900 30  0000 C CNN
	1    4850 1900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR11
U 1 1 5325818C
P 4850 2150
F 0 "#PWR11" H 4850 2150 30  0001 C CNN
F 1 "GND" H 4850 2080 30  0001 C CNN
F 2 "" H 4850 2150 60  0000 C CNN
F 3 "" H 4850 2150 60  0000 C CNN
	1    4850 2150
	1    0    0    -1  
$EndComp
$Comp
L R R52
U 1 1 53258192
P 5500 1400
F 0 "R52" V 5580 1400 40  0000 C CNN
F 1 "R" V 5507 1401 40  0000 C CNN
F 2 "~" V 5430 1400 30  0000 C CNN
F 3 "~" H 5500 1400 30  0000 C CNN
	1    5500 1400
	1    0    0    -1  
$EndComp
$Comp
L R R51
U 1 1 53258198
P 5500 1900
F 0 "R51" V 5580 1900 40  0000 C CNN
F 1 "R" V 5507 1901 40  0000 C CNN
F 2 "~" V 5430 1900 30  0000 C CNN
F 3 "~" H 5500 1900 30  0000 C CNN
	1    5500 1900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR13
U 1 1 5325819E
P 5500 2150
F 0 "#PWR13" H 5500 2150 30  0001 C CNN
F 1 "GND" H 5500 2080 30  0001 C CNN
F 2 "" H 5500 2150 60  0000 C CNN
F 3 "" H 5500 2150 60  0000 C CNN
	1    5500 2150
	1    0    0    -1  
$EndComp
$Comp
L ATTINY461A-S IC1
U 1 1 53258261
P 3350 5150
F 0 "IC1" H 2450 6100 40  0000 C CNN
F 1 "ATTINY461A-S" H 4100 4200 40  0000 C CNN
F 2 "SO20" H 3350 5150 35  0000 C CIN
F 3 "~" H 3350 5150 60  0000 C CNN
	1    3350 5150
	1    0    0    -1  
$EndComp
Text GLabel 7700 1350 2    60   Output ~ 0
BAT_0
Text GLabel 8300 1350 2    60   Output ~ 0
BAT_1
Text GLabel 8900 1350 2    60   Output ~ 0
BAT_2
Text GLabel 9500 1350 2    60   Output ~ 0
BAT_3
Text GLabel 10100 1350 2    60   Output ~ 0
BAT_4
Text GLabel 10750 1350 2    60   Output ~ 0
BAT_5
Wire Wire Line
	10750 1350 10650 1350
Wire Wire Line
	10650 1350 10650 1550
Wire Wire Line
	10100 1350 10000 1350
Wire Wire Line
	10000 1350 10000 1550
Wire Wire Line
	9500 1350 9400 1350
Wire Wire Line
	9400 1350 9400 1550
Wire Wire Line
	8900 1350 8800 1350
Wire Wire Line
	8800 1350 8800 1550
Wire Wire Line
	8300 1350 8200 1350
Wire Wire Line
	8200 1350 8200 1550
Wire Wire Line
	7600 1350 7700 1350
Wire Wire Line
	7600 1350 7600 1550
Text GLabel 2300 1150 1    60   Input ~ 0
BAT_0
Text GLabel 2850 1150 1    60   Input ~ 0
BAT_1
Text GLabel 3500 1150 1    60   Input ~ 0
BAT_2
Text GLabel 4250 1150 1    60   Input ~ 0
BAT_3
Text GLabel 4850 1150 1    60   Input ~ 0
BAT_4
Text GLabel 5500 1150 1    60   Input ~ 0
BAT_5
Text GLabel 1350 1100 0    60   Input ~ 0
Alim
Text GLabel 2350 1650 2    60   Output ~ 0
SENSE_0
Wire Wire Line
	2350 1650 2300 1650
Text GLabel 2900 1650 2    60   Output ~ 0
SENSE_1
Text GLabel 3550 1650 2    60   Output ~ 0
SENSE_2
Text GLabel 4350 1650 2    60   Output ~ 0
SENSE_3
Text GLabel 4950 1650 2    60   Output ~ 0
SENSE_4
Text GLabel 5600 1650 2    60   Output ~ 0
SENSE_5
Wire Wire Line
	5600 1650 5500 1650
Wire Wire Line
	4950 1650 4850 1650
Wire Wire Line
	4350 1650 4250 1650
Wire Wire Line
	3550 1650 3500 1650
Wire Wire Line
	2900 1650 2850 1650
Wire Wire Line
	1350 1600 1400 1600
Text GLabel 1400 1600 2    60   Output ~ 0
SENSE_Alim
Wire Wire Line
	2250 3900 2250 4750
Wire Wire Line
	2250 3900 2000 3900
Connection ~ 2250 4350
Text GLabel 2000 3900 0    60   Input ~ 0
VCC
Wire Wire Line
	2250 5550 2250 5950
Wire Wire Line
	2250 5950 2200 5950
Wire Wire Line
	2200 5950 2200 6000
$Comp
L GND #PWR4
U 1 1 5336EADD
P 2200 6000
F 0 "#PWR4" H 2200 6000 30  0001 C CNN
F 1 "GND" H 2200 5930 30  0001 C CNN
F 2 "" H 2200 6000 60  0000 C CNN
F 3 "" H 2200 6000 60  0000 C CNN
	1    2200 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 4650 5400 4650
Wire Wire Line
	5400 4650 5400 4700
$Comp
L C C1
U 1 1 5336EB0C
P 5400 4900
F 0 "C1" H 5400 5000 40  0000 L CNN
F 1 "100n" H 5406 4815 40  0000 L CNN
F 2 "~" H 5438 4750 30  0000 C CNN
F 3 "~" H 5400 4900 60  0000 C CNN
	1    5400 4900
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR12
U 1 1 5336EB1B
P 5400 5100
F 0 "#PWR12" H 5400 5100 30  0001 C CNN
F 1 "GND" H 5400 5030 30  0001 C CNN
F 2 "" H 5400 5100 60  0000 C CNN
F 3 "" H 5400 5100 60  0000 C CNN
	1    5400 5100
	1    0    0    -1  
$EndComp
Text GLabel 4450 4350 2    60   Input ~ 0
SENSE_0
Text GLabel 4450 4450 2    60   Input ~ 0
SENSE_1
Text GLabel 4450 4550 2    60   Input ~ 0
SENSE_2
Text GLabel 4450 4750 2    60   Input ~ 0
SENSE_3
Text GLabel 4450 4850 2    60   Input ~ 0
SENSE_4
Text GLabel 4450 4950 2    60   Input ~ 0
SENSE_5
Text GLabel 4450 5050 2    60   Input ~ 0
SENSE_Alim
Text GLabel 5950 5950 2    60   Output ~ 0
drv5
Text GLabel 5950 5850 2    60   Output ~ 0
drv4
Text GLabel 5950 5750 2    60   Output ~ 0
drv3
Text GLabel 5950 5650 2    60   Output ~ 0
drv2
Text GLabel 5950 5550 2    60   Output ~ 0
drv1
Text GLabel 5950 5450 2    60   Output ~ 0
drv0
$Comp
L R Rdrv0
U 1 1 5336EB4C
P 5700 5450
F 0 "Rdrv0" V 5780 5450 40  0000 C CNN
F 1 "R" V 5707 5451 40  0000 C CNN
F 2 "~" V 5630 5450 30  0000 C CNN
F 3 "~" H 5700 5450 30  0000 C CNN
	1    5700 5450
	0    -1   -1   0   
$EndComp
$Comp
L R Rdrv1
U 1 1 5336EB65
P 5700 5550
F 0 "Rdrv1" V 5780 5550 40  0000 C CNN
F 1 "R" V 5707 5551 40  0000 C CNN
F 2 "~" V 5630 5550 30  0000 C CNN
F 3 "~" H 5700 5550 30  0000 C CNN
	1    5700 5550
	0    -1   -1   0   
$EndComp
$Comp
L R Rdrv2
U 1 1 5336EB6B
P 5700 5650
F 0 "Rdrv2" V 5780 5650 40  0000 C CNN
F 1 "R" V 5707 5651 40  0000 C CNN
F 2 "~" V 5630 5650 30  0000 C CNN
F 3 "~" H 5700 5650 30  0000 C CNN
	1    5700 5650
	0    -1   -1   0   
$EndComp
$Comp
L R Rdrv3
U 1 1 5336EB71
P 5700 5750
F 0 "Rdrv3" V 5780 5750 40  0000 C CNN
F 1 "R" V 5707 5751 40  0000 C CNN
F 2 "~" V 5630 5750 30  0000 C CNN
F 3 "~" H 5700 5750 30  0000 C CNN
	1    5700 5750
	0    -1   -1   0   
$EndComp
$Comp
L R Rdrv4
U 1 1 5336EB77
P 5700 5850
F 0 "Rdrv4" V 5780 5850 40  0000 C CNN
F 1 "R" V 5707 5851 40  0000 C CNN
F 2 "~" V 5630 5850 30  0000 C CNN
F 3 "~" H 5700 5850 30  0000 C CNN
	1    5700 5850
	0    -1   -1   0   
$EndComp
$Comp
L R Rdrv5
U 1 1 5336EB7D
P 5700 5950
F 0 "Rdrv5" V 5780 5950 40  0000 C CNN
F 1 "R" V 5707 5951 40  0000 C CNN
F 2 "~" V 5630 5950 30  0000 C CNN
F 3 "~" H 5700 5950 30  0000 C CNN
	1    5700 5950
	0    -1   -1   0   
$EndComp
Text GLabel 4450 5250 2    60   BiDi ~ 0
MOSI
Text GLabel 4450 5350 2    60   BiDi ~ 0
MISO
Text GLabel 4450 5450 2    60   BiDi ~ 0
SCK
Text GLabel 4450 5550 2    60   BiDi ~ 0
PB3
Text GLabel 4450 5650 2    60   BiDi ~ 0
PB4
Text GLabel 4450 5750 2    60   BiDi ~ 0
PB5
Text GLabel 4450 5850 2    60   BiDi ~ 0
PB6
Text GLabel 4450 5950 2    60   BiDi ~ 0
RESET
$Comp
L AVR-ISP-6 CON1
U 1 1 5336EB8A
P 1750 7000
F 0 "CON1" H 1670 7240 50  0000 C CNN
F 1 "AVR-ISP-6" H 1510 6770 50  0000 L BNN
F 2 "AVR-ISP-6" V 1230 7040 50  0001 C CNN
F 3 "~" H 1750 7000 60  0000 C CNN
	1    1750 7000
	1    0    0    -1  
$EndComp
Text GLabel 1300 6850 0    60   BiDi ~ 0
MISO
Text GLabel 1300 7000 0    60   BiDi ~ 0
SCK
Text GLabel 1300 7150 0    60   BiDi ~ 0
RESET
Text GLabel 2200 6850 2    60   BiDi ~ 0
VCC
Text GLabel 2200 7000 2    60   BiDi ~ 0
MOSI
$Comp
L GND #PWR5
U 1 1 5336EB9C
P 2250 7150
F 0 "#PWR5" H 2250 7150 30  0001 C CNN
F 1 "GND" H 2250 7080 30  0001 C CNN
F 2 "" H 2250 7150 60  0000 C CNN
F 3 "" H 2250 7150 60  0000 C CNN
	1    2250 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2200 6850 1875 6850
Wire Wire Line
	1875 6850 1875 6900
Wire Wire Line
	2200 7000 1875 7000
Wire Wire Line
	2250 7150 2250 7100
Wire Wire Line
	2250 7100 1875 7100
Wire Wire Line
	1300 7150 1625 7150
Wire Wire Line
	1625 7150 1625 7100
Wire Wire Line
	1300 7000 1625 7000
Wire Wire Line
	1300 6850 1625 6850
Wire Wire Line
	1625 6850 1625 6900
Text GLabel 5450 5950 0    60   Input ~ 0
PB5
Text GLabel 5450 5850 0    60   Input ~ 0
PB4
Text GLabel 5450 5750 0    60   Input ~ 0
PB3
Text GLabel 5450 5650 0    60   Input ~ 0
SCK
Text GLabel 5450 5550 0    60   Input ~ 0
MISO
Text GLabel 5450 5450 0    60   Input ~ 0
MOSI
$Comp
L 7805 U1
U 1 1 5336ED26
P 4700 6900
F 0 "U1" H 4850 6704 60  0000 C CNN
F 1 "7805" H 4700 7100 60  0000 C CNN
F 2 "~" H 4700 6900 60  0000 C CNN
F 3 "~" H 4700 6900 60  0000 C CNN
	1    4700 6900
	1    0    0    -1  
$EndComp
Text GLabel 4275 6850 0    60   Input ~ 0
ALIM
Wire Wire Line
	4275 6850 4300 6850
Wire Wire Line
	5100 6850 5450 6850
Text GLabel 5450 6850 2    60   Output ~ 0
VCC
Wire Wire Line
	4700 7150 4700 7275
$Comp
L GND #PWR10
U 1 1 5336EDE8
P 4700 7275
F 0 "#PWR10" H 4700 7275 30  0001 C CNN
F 1 "GND" H 4700 7205 30  0001 C CNN
F 2 "" H 4700 7275 60  0000 C CNN
F 3 "" H 4700 7275 60  0000 C CNN
	1    4700 7275
	1    0    0    -1  
$EndComp
$Comp
L C Ca1
U 1 1 5336EDF5
P 4300 7050
F 0 "Ca1" H 4300 7150 40  0000 L CNN
F 1 "47u" H 4306 6965 40  0000 L CNN
F 2 "~" H 4338 6900 30  0000 C CNN
F 3 "~" H 4300 7050 60  0000 C CNN
	1    4300 7050
	1    0    0    -1  
$EndComp
$Comp
L C Ca2
U 1 1 5336EDFB
P 5100 7050
F 0 "Ca2" H 5100 7150 40  0000 L CNN
F 1 "10u" H 5106 6965 40  0000 L CNN
F 2 "~" H 5138 6900 30  0000 C CNN
F 3 "~" H 5100 7050 60  0000 C CNN
	1    5100 7050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 7250 5100 7250
Connection ~ 4700 7250
Wire Wire Line
	10775 1950 10775 2025
Connection ~ 10650 1950
$Comp
L GND #PWR16
U 1 1 5336F4B8
P 10775 2025
F 0 "#PWR16" H 10775 2025 30  0001 C CNN
F 1 "GND" H 10775 1955 30  0001 C CNN
F 2 "" H 10775 2025 60  0000 C CNN
F 3 "" H 10775 2025 60  0000 C CNN
	1    10775 2025
	1    0    0    -1  
$EndComp
Text GLabel 8825 3525 0    60   BiDi ~ 0
BAT_0
Text GLabel 8825 3700 0    60   BiDi ~ 0
BAT_1
Text GLabel 8825 3875 0    60   BiDi ~ 0
BAT_2
Text GLabel 8825 4050 0    60   BiDi ~ 0
BAT_3
Text GLabel 8825 4225 0    60   BiDi ~ 0
BAT_4
Text GLabel 8825 4400 0    60   BiDi ~ 0
BAT_5
$Comp
L R Rr1
U 1 1 53370169
P 8250 5275
F 0 "Rr1" V 8330 5275 40  0000 C CNN
F 1 "R" V 8257 5276 40  0000 C CNN
F 2 "~" V 8180 5275 30  0000 C CNN
F 3 "~" H 8250 5275 30  0000 C CNN
	1    8250 5275
	0    -1   -1   0   
$EndComp
$Comp
L C Cr1
U 1 1 53370178
P 8500 5475
F 0 "Cr1" H 8500 5575 40  0000 L CNN
F 1 "C" H 8506 5390 40  0000 L CNN
F 2 "~" H 8538 5325 30  0000 C CNN
F 3 "~" H 8500 5475 60  0000 C CNN
	1    8500 5475
	1    0    0    -1  
$EndComp
Text GLabel 8000 5275 0    60   Input ~ 0
VCC
$Comp
L GND #PWR14
U 1 1 53370191
P 8500 5675
F 0 "#PWR14" H 8500 5675 30  0001 C CNN
F 1 "GND" H 8500 5605 30  0001 C CNN
F 2 "" H 8500 5675 60  0000 C CNN
F 3 "" H 8500 5675 60  0000 C CNN
	1    8500 5675
	1    0    0    -1  
$EndComp
Text GLabel 8500 5275 2    60   BiDi ~ 0
RESET
$Comp
L CONN_2 P1
U 1 1 5337027D
P 1325 3025
F 0 "P1" V 1575 3125 40  0000 C CNN
F 1 "CONN_2" H 1375 3025 40  0000 C CNN
F 2 "~" H 1325 3025 60  0000 C CNN
F 3 "~" H 1325 3025 60  0000 C CNN
	1    1325 3025
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR3
U 1 1 53370297
P 1675 3125
F 0 "#PWR3" H 1675 3125 30  0001 C CNN
F 1 "GND" H 1675 3055 30  0001 C CNN
F 2 "" H 1675 3125 60  0000 C CNN
F 3 "" H 1675 3125 60  0000 C CNN
	1    1675 3125
	1    0    0    -1  
$EndComp
Text GLabel 1675 2925 2    60   Output ~ 0
Alim
$Comp
L R Rd1
U 1 1 53370699
P 9775 5250
F 0 "Rd1" V 9855 5250 40  0000 C CNN
F 1 "R" V 9782 5251 40  0000 C CNN
F 2 "~" V 9705 5250 30  0000 C CNN
F 3 "~" H 9775 5250 30  0000 C CNN
	1    9775 5250
	0    -1   -1   0   
$EndComp
Text GLabel 9525 5250 0    60   Input ~ 0
VCC
$Comp
L GND #PWR15
U 1 1 533706A6
P 10025 5650
F 0 "#PWR15" H 10025 5650 30  0001 C CNN
F 1 "GND" H 10025 5580 30  0001 C CNN
F 2 "" H 10025 5650 60  0000 C CNN
F 3 "" H 10025 5650 60  0000 C CNN
	1    10025 5650
	1    0    0    -1  
$EndComp
$Comp
L LED D1
U 1 1 533706AF
P 10025 5450
F 0 "D1" H 10025 5550 50  0000 C CNN
F 1 "LED" H 10025 5350 50  0000 C CNN
F 2 "~" H 10025 5450 60  0000 C CNN
F 3 "~" H 10025 5450 60  0000 C CNN
	1    10025 5450
	0    1    1    0   
$EndComp
$Comp
L CONN_3 K1
U 1 1 533748DD
P 9325 3650
F 0 "K1" V 9275 3650 50  0000 C CNN
F 1 "CONN_3" V 9375 3650 40  0000 C CNN
F 2 "~" H 9325 3650 60  0000 C CNN
F 3 "~" H 9325 3650 60  0000 C CNN
	1    9325 3650
	1    0    0    -1  
$EndComp
$Comp
L CONN_3 K2
U 1 1 533748EA
P 9325 4225
F 0 "K2" V 9275 4225 50  0000 C CNN
F 1 "CONN_3" V 9375 4225 40  0000 C CNN
F 2 "~" H 9325 4225 60  0000 C CNN
F 3 "~" H 9325 4225 60  0000 C CNN
	1    9325 4225
	1    0    0    -1  
$EndComp
$Comp
L CONN_3 K3
U 1 1 533748F0
P 10575 3525
F 0 "K3" V 10525 3525 50  0000 C CNN
F 1 "CONN_3" V 10625 3525 40  0000 C CNN
F 2 "~" H 10575 3525 60  0000 C CNN
F 3 "~" H 10575 3525 60  0000 C CNN
	1    10575 3525
	1    0    0    -1  
$EndComp
$Comp
L CONN_3 K4
U 1 1 533748F6
P 10575 3875
F 0 "K4" V 10525 3875 50  0000 C CNN
F 1 "CONN_3" V 10625 3875 40  0000 C CNN
F 2 "~" H 10575 3875 60  0000 C CNN
F 3 "~" H 10575 3875 60  0000 C CNN
	1    10575 3875
	1    0    0    -1  
$EndComp
Wire Wire Line
	8975 3550 8825 3550
Wire Wire Line
	8825 3550 8825 3525
Wire Wire Line
	8975 3650 8825 3650
Wire Wire Line
	8825 3650 8825 3700
Wire Wire Line
	8975 3750 8975 3850
Wire Wire Line
	8975 3850 8825 3850
Wire Wire Line
	8825 3850 8825 3875
Wire Wire Line
	8975 4125 8825 4125
Wire Wire Line
	8825 4125 8825 4050
Wire Wire Line
	8825 4225 8975 4225
Wire Wire Line
	8975 4325 8825 4325
Wire Wire Line
	8825 4325 8825 4400
Wire Wire Line
	10225 3425 10225 3975
Text GLabel 10225 3425 0    60   Input ~ 0
ALIM
Connection ~ 10225 3425
Connection ~ 10225 3525
Connection ~ 10225 3625
Connection ~ 10225 3775
Connection ~ 10225 3875
Connection ~ 10225 3975
$Comp
L CONN_2 P2
U 1 1 53374D7E
P 1275 3425
F 0 "P2" V 1525 3525 40  0000 C CNN
F 1 "CONN_2" H 1325 3425 40  0000 C CNN
F 2 "~" H 1275 3425 60  0000 C CNN
F 3 "~" H 1275 3425 60  0000 C CNN
	1    1275 3425
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR2
U 1 1 53374D84
P 1625 3525
F 0 "#PWR2" H 1625 3525 30  0001 C CNN
F 1 "GND" H 1625 3455 30  0001 C CNN
F 2 "" H 1625 3525 60  0000 C CNN
F 3 "" H 1625 3525 60  0000 C CNN
	1    1625 3525
	1    0    0    -1  
$EndComp
Text GLabel 1625 3325 2    60   Output ~ 0
Alim
$Comp
L C C2
U 1 1 5339A051
P 2100 5150
F 0 "C2" H 2100 5250 40  0000 L CNN
F 1 "C" H 2106 5065 40  0000 L CNN
F 2 "~" H 2138 5000 30  0000 C CNN
F 3 "~" H 2100 5150 60  0000 C CNN
	1    2100 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2250 4750 2100 4750
Wire Wire Line
	2100 4750 2100 4950
Wire Wire Line
	2100 5350 2100 5550
Wire Wire Line
	2100 5550 2250 5550
$EndSCHEMATC
